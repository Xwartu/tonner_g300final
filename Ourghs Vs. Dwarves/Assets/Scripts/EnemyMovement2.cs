﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyMovement2 : MonoBehaviour
{
    public Transform enemyGraphic;
    private Transform target;
    private int wavepointIndex = 0;

    private Enemy enemy;
    private WaveSpawner waveSpawner;

    void Start()
    {
        enemy = GetComponent<Enemy>(); 

        target = Waypoints2.points2[0];
    }

    void Update()
    {
        Vector3 dir = target.position - transform.position;
        
        transform.Translate(dir.normalized * enemy.speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWaypoint();
        }

        enemy.speed = enemy.startSpeed;
    }

    void GetNextWaypoint()
    {
        if (wavepointIndex >= Waypoints2.points2.Length - 1)
        {
            EndOfPath();
            return;
        }

        wavepointIndex++;
        target = Waypoints2.points2[wavepointIndex];
        enemyGraphic.LookAt(target);
    }

    void EndOfPath()
    {
        PlayerStats.Lives--;
        WaveSpawner2.EnemiesAlive--;
        Destroy(gameObject);
    }
}
