﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LivesChecker : MonoBehaviour
{
    public bool gameEnded = false;
    public bool gamePaused = true;

    public int spawnerCount = 0;
    public int ListCount = 0;
    public AudioSource audioSource;
    public AudioSource audioSource2;

    public GameObject GameEnd;
    public GameObject GameWin;

    public List<Enemy> enemyList;
    public List<Turret> turretList;
    public List<Node> nodeList;

    public PlayerStats playerStats;

    private void Start()
    {
        spawnerCount = 0;
        gamePaused = false;
        gameEnded = false;
        Time.timeScale = 1f;
        GameEnd.SetActive(false);
        enemyList = new List<Enemy>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            gamePaused = !gamePaused;
        }
        PauseGame();

        if (gameEnded)
        {
            return;
        }

        if (PlayerStats.Lives <= 0)
        {
            audioSource.Play();
            EndGame();
        }
    }

    void PauseGame()
    {
        if(gamePaused == false)
        {
            Time.timeScale = 1f;
            return;
        }
        if (gamePaused == true)
        {
            Time.timeScale = 0f;
            return;
        }
    }

    public void WinGame()
    {
        if (spawnerCount == 4) 
        {
            //gameEnded = true;
            GameWin.SetActive(true);
            audioSource2.Play();
            Time.timeScale = 0f;
        }
        else
        {
            return;
        }
    }

    void EndGame()
    {
        //gameEnded = true;
        GameEnd.SetActive(true);
        Time.timeScale = 0f;

    }

    public void RestartGame()
    {
        playerStats.Start();
        StartCoroutine(Restarting());
        audioSource.Stop();
        audioSource2.Stop();
        Start();
    }

    IEnumerator Restarting()
    {
        foreach (Turret turret in turretList.ToList())
        {
            Destroy(turret.gameObject);
            Debug.Log("Turret's Cleared");
        }

        foreach (Node node in nodeList.ToList())
        {
            node.NodeReset();
            Debug.Log("Node's Cleared");
        }

        foreach (Enemy enemy in enemyList.ToList()) 
        {
            Destroy(enemy.gameObject);
            Debug.Log("Enemy deleted");

        }
        yield return new WaitUntil(() => ListCount == 0);
    }
}




