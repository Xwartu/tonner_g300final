﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Enemy : MonoBehaviour
{
    public float startSpeed = 10f;
    private float health;
    public Enemy enemySelf;

    [HideInInspector]
    public float speed;

    public float startHealth = 100;
    public int worth = 50;
    public GameObject deathEffect;

    public Image healthBar;
    public AudioSource Audio;

    void Start()
    {
        health = startHealth;
        speed = startSpeed;
        BuildManager.instance.enemy = enemySelf;
        BuildManager.instance.lc.enemyList.Add(this);
        BuildManager.instance.lc.ListCount++;
        Debug.Log("Enemy Added");
    }

    public void TakeDamage(float amount)
    {
        health -= amount;

        healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();
        }
    }

    public void Slow(float pct)
    {
        speed = startSpeed * (1f - pct);
    }

    void Die()
    {
        PlayerStats.Money += worth;

        GameObject effect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(effect, 5f);


        WaveSpawner.EnemiesAlive--;

        Audio.Play();
        Destroy(gameObject);
    }

    void OnDestroy()
    {
        BuildManager.instance.lc.enemyList.Remove(this);
        BuildManager.instance.lc.ListCount--;
        Debug.Log("Enemy Removed");
    }
}
