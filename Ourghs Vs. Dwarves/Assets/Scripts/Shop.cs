﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBlueprint standardTurret;
    public TurretBlueprint missileLauncher;
    public TurretBlueprint laserBeamer;
    public TurretBlueprint sniperTower;

    BuildManager buildManager;

    void Start()
    {
        buildManager = BuildManager.instance;
    }

    public void SelectStandardTurret()
    {
        Debug.Log("Standard Turret Selected");
        buildManager.SelectTurretToBuild(standardTurret);
    }

    public void SelectBoomCannon()
    {
        Debug.Log("Boom Cannon Selected");
        buildManager.SelectTurretToBuild(missileLauncher);
    }

    public void SelectWizardTower()
    {
        Debug.Log("WizardTower Selected");
        buildManager.SelectTurretToBuild(laserBeamer);
    }

    public void SelectSniperTower()
    {
        Debug.Log("Sniper Selected");
        buildManager.SelectTurretToBuild(sniperTower);
    }
}
